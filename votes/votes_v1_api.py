from flask import Blueprint, request, abort
from flask_restful import Resource, fields, marshal_with, Api, inputs
from flask_restful.reqparse import RequestParser

from votes.vote import Vote

_VOTE_FIELDS = dict(
    parent_uuid=fields.String,
    user_uuid=fields.String,
    created=fields.DateTime("iso8601"),
    positive=fields.Boolean,
)


class VotesResource(Resource):

    CREATE_ARGS = RequestParser()
    CREATE_ARGS.add_argument("positive", type=inputs.boolean, required=True)

    @marshal_with(_VOTE_FIELDS)
    def post(self, parent_uuid: str) -> Vote:
        user_uuid: str = request.remote_user or abort(401)

        args = self.CREATE_ARGS.parse_args()
        vote = Vote(
            parent_uuid=parent_uuid, user_uuid=user_uuid, positive=args["positive"]
        )
        vote.save()
        return vote

    @marshal_with(_VOTE_FIELDS)
    def get(self, parent_uuid: str) -> Vote:
        user_uuid: str = request.remote_user or abort(401)

        try:
            vote = Vote.get(parent_uuid, user_uuid)
            return vote
        except Vote.DoesNotExist:
            vote = Vote(parent_uuid=parent_uuid, user_uuid=user_uuid)
            vote.save()
            return vote

    @marshal_with(_VOTE_FIELDS)
    def delete(self, parent_uuid: str):
        user_uuid: str = request.remote_user or abort(401)

        vote = Vote.get(parent_uuid, user_uuid)
        vote.delete()


def create_blueprint(votes_table_name: str):
    Vote.Meta.table_name = votes_table_name

    bp = Blueprint("votes-1", __name__)
    api = Api(bp)
    api.add_resource(VotesResource, "/<string:parent_uuid>")
    return bp

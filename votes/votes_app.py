from flask import Flask
from votes.votes_v1_api import create_blueprint


def create_app(votes_table_name: str) -> Flask:
    app = Flask(__name__)
    v1_bp = create_blueprint(votes_table_name)
    app.register_blueprint(v1_bp, url_prefix="/votes/1")
    return app

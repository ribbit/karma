from uuid import uuid4
from datetime import datetime
from typing import Optional

from pynamodb.models import Model
from pynamodb.attributes import UnicodeAttribute, UTCDateTimeAttribute, BooleanAttribute


class Vote(Model):
    class Meta:
        table_name: Optional[str] = None

    parent_uuid = UnicodeAttribute(hash_key=True, default=lambda: str(uuid4()))
    user_uuid = UnicodeAttribute(range_key=True)
    created = UTCDateTimeAttribute(null=False, default=lambda: datetime.utcnow())
    positive = BooleanAttribute(null=True)

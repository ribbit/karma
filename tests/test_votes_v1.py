from os import environ

import pytest
from moto import mock_dynamodb2

from votes.vote import Vote
from tests.votes_local_driver import VotesDriver as Driver
from votes.votes_app import create_app


@pytest.fixture(name="client")
def client_fixture():
    app = create_app('votes')
    app.config["TESTING"] = True
    app.config["DEBUG"] = True

    environ['AWS_ACCESS_KEY_ID'] = 'foobar_id'
    environ['AWS_SECRET_ACCESS_KEY'] = 'foobar_secret'
    with mock_dynamodb2():
        Vote.create_table(read_capacity_units=1, write_capacity_units=1)
        yield app.test_client()


@pytest.fixture(name="driver1")
def driver1_fixture(client) -> Driver:
    return Driver(client, "user1")


@pytest.fixture(name="driver2")
def driver2_fixture(client) -> Driver:
    return Driver(client, "user2")


def test_upvote(driver1: Driver):
    vote = driver1.when_vote("parent1", True)

    assert vote["positive"] is True


def test_downvote(driver1: Driver):
    vote = driver1.when_vote("parent1", False)

    assert vote["positive"] is False


def test_get_upvote(driver1: Driver):
    driver1.when_vote("parent1", True)

    assert driver1.then_get("parent1") is True


def test_delete_upvote(driver1: Driver):
    driver1.when_vote("parent1", True)
    driver1.when_delete("parent1")

    assert driver1.then_get("parent1") is None


def test_get_deleted_vote(driver1: Driver):
    assert driver1.then_get("parent1") is None


def test_upvote_downvote(driver1: Driver):
    driver1.when_vote("parent1", True)
    driver1.when_vote("parent1", False)

    assert driver1.then_get("parent1") is False


def test_multi_user_votes(driver1: Driver, driver2: Driver):
    driver1.when_vote("parent1", True)
    driver2.when_vote("parent1", False)

    assert driver1.then_get("parent1") is True
    assert driver2.then_get("parent1") is False


def test_vote_unauthorized(client):
    r = client.post("/votes/1/parent1", data=dict(positive=True))

    assert r.status_code == 401


def test_get_unauthorized(client):
    r = client.get("/votes/1/parent1")
    assert r.status_code == 401


def test_delete_unauthorized(client):
    r = client.delete("/votes/1/parent1")
    assert r.status_code == 401


def test_vote_invalid(client):
    r = client.post("/votes/1/parent1", environ_base=dict(REMOTE_USER="user1"))

    assert r.status_code == 400

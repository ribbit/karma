from typing import Optional


class VotesDriver(object):
    def __init__(self, client, user_uuid: str):
        self.client = client
        self.user_uuid = user_uuid

    def when_vote(self, parent_uuid: str, positive: bool) -> dict:
        r = self.client.post(
            f"/votes/1/{parent_uuid}",
            environ_base=dict(REMOTE_USER=self.user_uuid),
            data=dict(positive=positive),
        )
        assert 200 == r.status_code

        body = r.get_json()
        assert body["parent_uuid"] == parent_uuid
        assert body["user_uuid"] == self.user_uuid
        assert body["positive"] == positive
        assert body["created"] is not None
        return body

    def when_delete(self, parent_uuid: str):
        r = self.client.delete(
            f"/votes/1/{parent_uuid}", environ_base=dict(REMOTE_USER=self.user_uuid)
        )
        assert r.status_code == 200

    def then_get(self, parent_uuid: str) -> Optional[bool]:
        r = self.client.get(
            f"/votes/1/{parent_uuid}", environ_base=dict(REMOTE_USER=self.user_uuid)
        )
        assert r.status_code == 200, r.get_json()

        body = r.get_json()
        assert body["user_uuid"] == self.user_uuid
        assert body["parent_uuid"] == parent_uuid
        assert body["created"] is not None
        return body["positive"]

from os import environ

from google.oauth2.id_token import verify_oauth2_token
import requests
from pyauthlib import UserInfo, AuthPolicy, HttpMethod, parse_event, raise_401

GOOGLE_CLIENT_ID = environ['GOOGLE_CLIENT_ID']


def verify_google_id_token(id_token: str) -> dict:
    user_info = verify_oauth2_token(id_token, requests.Request(), GOOGLE_CLIENT_ID)
    if user_info['iss'] not in ['accounts.google.com', 'https://accounts.google.com']:
        raise ValueError('Wrong issuer.')
    return user_info


def lambda_handler(event, _context):
    event = parse_event(event)

    identity = verify_google_id_token(event.access_token)
    user_info = UserInfo(identity['user_id'], identity['grants'])
    policy = AuthPolicy(user_info)

    if not user_info:
        raise_401()
    else:
        policy.allow(event.arn(method=HttpMethod.ALL, resource='*'))

    return policy.build()
